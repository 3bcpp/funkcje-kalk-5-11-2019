/*
 * ZADANIE 1:
 * Dotworzyć do kalkulatora wszystkie poznane operacje arytmetyczne
 * z wykorzystaniem już zaproponowanych funkcji
 *
 * ZADANIE 2:
 * Zaproponować funkcję, dla wyliczania potęg i silni (bez cmath)
 *
 * ZADANIE 3:
 * Zaproponować rozwiązanie, dzięki któremu jeżeli użytkownik wybierze
 * operację wymagająca jednej liczby program poprosi go tylko o jedną liczbę
 * (nie jak dotychczas o dwie)
 *
 * ZADANIE 4:
 * Sprobowac, poprzez wykorzystanie funkcji i jej parametrow
 * obliczyc modul z podawanych przez uzytkownika wartosci float.
 *
*/

#include <iostream>
#include <string>

//TO JEST DEKLARACJA!!
//funkckja musi być co najmniej zadeklarowana przed pierwszym użyciem!
//może być oczywiście od razu zdefiniowana
//ta funkcja będzie przyjmować 3 parametry
// float a - będzie umożliwiać przekazanie do funkcji zmiennej a
// float b - będzie umożliwiać przekazanie do funkcji zmiennej b
// char o - będzie umożliwiać przekazanie do funkcji kodu operacji
void dzialaj(float a, float b, char o);

//TO JEST DEKLARACJA I DEFINICJA!
//ta funkcja zwraca wartosc przy swoim zakonczeniu
//funkcja nie posiada parametrów wejściowych
//parametry to nic innego jak zmienne, które przyjmuje funkcja i może z nich korzystać
//jak ze zmiennych.
char menu() {
    char op='\0';
    std::cout << "Podaj kod operacji: ";
    std::cin >> op;
    //w przypadku funkcji zwracajacych wartosci return jest OBLIGATORYJNE
    return op;
}

int main()
{
    while(1) {
        char op = menu();
        if (op=='q') {
            break;
        }
        float a = .0f,
              b = .0f;
        std::cout << "Podaj a: ";
        std::cin >> a;
        std::cout << "Podaj b: ";
        std::cin >> b;
        //tutaj funkcja przyjmuje podane przez uzytkownika zmienne jako
        //parametry do funkcji
        dzialaj(a,b,op);
        //przyklady wywolania funkcji z literalami (stalymi wartosciami zapisanymi w kodzie)
        //literaly zostana zamienione za parametry-zmienne funkcji i zostana potraktowane
        //jako wartosci zmienne
        //dzialaj(14,15,'-');
        //dzialaj(4,8,'/');

    }

    return 0;
}

//TO JEST DEFINICJA FUNKCJI! (jej ciało)
void dzialaj(float pierwsza, float druga, char operacja) {

    float wynik=.0f;
    switch(operacja) {
        case '+': wynik=pierwsza+druga; break;
        case '-': wynik=pierwsza-druga; break;
        case '*': wynik=pierwsza*druga; break;
        case '/': wynik=pierwsza/druga; break;
        //return zwsze powoduje natychmiastowe zakonczenie wykonywaje funkcji
        //nie jest to rownoznaczne z zakonczeniem programu (tylko w main return konczy program)
        //case 'q': return ;
        //ta funkcja ma za zadanie zakonczyc program zwracajac do systemu operacyjnego
        //kod podany jako parametr
        case 'q': exit(0);
        default: std::cout << "Operacja nie zostala rozpoznana!";
    }
    std::cout << "Dzialanie " << pierwsza << ' ' << operacja
              << ' ' << druga << " = " << wynik << std::endl;
}
